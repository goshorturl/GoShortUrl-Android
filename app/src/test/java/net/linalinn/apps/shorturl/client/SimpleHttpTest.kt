package net.linalinn.apps.shorturl.client

import net.linalinn.apps.shorturl.client.services.apis.SimpleHttp
import org.junit.Test

import java.net.URL

class SimpleHttpTest {

    @Test
    fun doPost() {
        val http = SimpleHttp
        println(http.doPost(URL("http://localhost:8001/"),"some data"))
    }
}