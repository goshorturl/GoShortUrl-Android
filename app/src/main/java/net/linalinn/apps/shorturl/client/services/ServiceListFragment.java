package net.linalinn.apps.shorturl.client.services;

import android.annotation.SuppressLint;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import net.linalinn.apps.shorturl.client.BlankFragment;
import net.linalinn.apps.shorturl.client.R;
import net.linalinn.apps.shorturl.client.services.database.ServiceAdapter;
import net.linalinn.apps.shorturl.client.services.database.ServiceDatabase;

import java.util.Arrays;

public class ServiceListFragment extends Fragment {
    public ServiceListFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_service_list, container, false);
        // Inflate the layout for this fragment
        RecyclerView recyclerView = rootView.findViewById(R.id.RecyclerView);
        ServiceDatabase db = Room.databaseBuilder(getContext(),ServiceDatabase.class,"services.db").allowMainThreadQueries().build();

        recyclerView.setAdapter(new ServiceAdapter(Arrays.asList(db.servicelDao().loadAllServices()),getFragmentManager()));
        LinearLayoutManager l = new LinearLayoutManager(getActivity());
        l.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(l);



        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((FloatingActionButton) getActivity().findViewById(R.id.fab)).setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.framentContainer,new BlankFragment());
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
    }
}
