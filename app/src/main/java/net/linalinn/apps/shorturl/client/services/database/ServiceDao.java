package net.linalinn.apps.shorturl.client.services.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface ServiceDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertServiceDao(Service... services);
    @Update
    public void updateServiceDao(Service... services);
    @Delete
    public void deleteServiceDao(Service... services);
    @Query("SELECT * FROM service")
    public Service[] loadAllServices();

}
