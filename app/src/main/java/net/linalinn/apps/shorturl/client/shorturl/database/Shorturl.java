package net.linalinn.apps.shorturl.client.shorturl.database;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

@Entity
public class Shorturl {
    @PrimaryKey @NotNull
    private String ShortUrl;
    private String Url;
    private String Service;
    private int clicks;

    public String getShortUrl() {
        return ShortUrl;
    }

    public String getUrl() {
        return Url;
    }


    public String getService() {
        return Service;
    }


    public int getClicks() {
        return clicks;
    }

    public void setShortUrl(String shortUrl) {
        ShortUrl = shortUrl;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public void setService(String service) {
        Service = service;
    }

    public void setClicks(int clicks) {
        this.clicks = clicks;
    }

    public Shorturl(String shortUrl, String url, String service, int clicks) {
        ShortUrl = shortUrl;
        Url = url;
        Service = service;
        this.clicks = clicks;
    }

    @NonNull
    @Override
    public String toString() {
        return "Shorturl:"+ShortUrl+" Clicks:"+Integer.toString(clicks)+" Service:"+Service+" URL:"+Url;
    }

    public Shorturl(){

    }
}
