package net.linalinn.apps.shorturl.client.services.database;

import androidx.room.Entity;

import org.jetbrains.annotations.NotNull;

@Entity(primaryKeys = {"Username","ServerUrl"})
public class Service {
    @NotNull
    private String Username;
    @NotNull
    private String ServerUrl;
    private String AccessToken;
    private int ApiType;

    @NotNull
    public String getUsername() {
        return Username;
    }

    public void setUsername(@NotNull String username) {
        this.Username = username;
    }

    public int getApiType() {
        return ApiType;
    }

    public void setApiType(int apiType) {
        ApiType = apiType;
    }

    public Service(@NotNull String username, String serverUrl, String accessToken, int apiType) {
        this.Username = username;
        ServerUrl = serverUrl;
        AccessToken = accessToken;
        ApiType = apiType;
    }

    public Service(@NotNull String username, int apiType) {
        this.Username = username;
        ApiType = apiType;
    }

    public String getServerUrl() {
        return ServerUrl;
    }

    public void setServerUrl(String serverUrl) {
        ServerUrl = serverUrl;
    }

    public String getAccessToken() {
        return AccessToken;
    }

    public void setAccessToken(String accessToken) {
        AccessToken = accessToken;
    }

    public Service() {
    }
}
