package net.linalinn.apps.shorturl.client.shorturl;


import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import net.linalinn.apps.shorturl.client.R;
import net.linalinn.apps.shorturl.client.shorturl.database.ShorturlAdapter;
import net.linalinn.apps.shorturl.client.shorturl.database.ShorturlDatabase;

import java.util.Arrays;


/**
 * A simple {@link Fragment} subclass.
 */
public class Shorturls extends Fragment {


    public Shorturls() {
        // Required empty public constructor
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_shorturls, container, false);


        ShorturlDatabase db  = Room.databaseBuilder(getContext().getApplicationContext(), ShorturlDatabase.class,"Shorturl.db").allowMainThreadQueries().build();
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.RecyclerView);
        Log.d("BDSIZE", Integer.toString(Arrays.asList(db.shorturlDao().loadAllShorturls()).size()));
        ShorturlAdapter adapter = new ShorturlAdapter(Arrays.asList(db.shorturlDao().loadAllShorturls()),getActivity().getSupportFragmentManager());
        Log.d("AdapterItemCount", Integer.toString(adapter.getItemCount()));
        recyclerView.setAdapter(adapter);
        LinearLayoutManager l = new LinearLayoutManager(getActivity());
        l.setOrientation(RecyclerView.VERTICAL);
        recyclerView.setLayoutManager(l);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        Log.d("recylerviewChildCount", Integer.toString(recyclerView.getChildCount()));
        Log.d("BDSIZE", "DONE");
        return  rootView;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public void onStart() {
        super.onStart();
        ((FloatingActionButton) getActivity().findViewById(R.id.fab)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.replace(R.id.framentContainer, new Short());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);



    }
}
