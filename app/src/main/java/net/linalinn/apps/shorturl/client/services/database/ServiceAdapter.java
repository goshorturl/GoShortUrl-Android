package net.linalinn.apps.shorturl.client.services.database;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import net.linalinn.apps.shorturl.client.R;
import net.linalinn.apps.shorturl.client.services.apis.Polrapi;
import net.linalinn.apps.shorturl.client.services.apis.SupportedApis;

import java.util.List;

public class ServiceAdapter extends  RecyclerView.Adapter<ServiceAdapter.ViewHolder> {
    private List<Service> Services;
    private FragmentManager fragmentManager;

    public ServiceAdapter(List<Service> services, FragmentManager fragmentManager) {
        Services = services;
        this.fragmentManager = fragmentManager;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        // Inflate the custom layout
        View serviceView = inflater.inflate(R.layout.item_service, parent, false);
        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(serviceView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Service service = Services.get(position);
        holder.service = service;
        holder.Username.setText(service.getUsername());
        holder.Service.setText(SupportedApis.getName(service.getApiType()));
        holder.Url.setText(service.getServerUrl());
    }

    @Override
    public int getItemCount() {
        return Services.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView Username;
        public TextView Url;
        public TextView Service;
        public Service service;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(SupportedApis.Polr == service.getApiType()){
                        try {
                            (new Polrapi()).update(service.getServerUrl(),service.getServerUrl());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    //
                }
            });
            Username = (TextView) itemView.findViewById(R.id.shorturl);
            Url = (TextView) itemView.findViewById(R.id.item_url);
            Service = (TextView) itemView.findViewById(R.id.item_service);

        }
    }
}


