package net.linalinn.apps.shorturl.client.services.apis

import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

object SimpleHttp {

    @Throws(Exception::class)
    @JvmStatic fun doPost(url: URL, data: String): HttpResponse {
        val con = url.openConnection() as HttpURLConnection
        con.doOutput = true
        con.requestMethod = "POST"
        con.setRequestProperty("User-Agent","GoShortUrl-Android")
        val httpRequestBodyWriter = BufferedWriter(OutputStreamWriter(con.outputStream))
        httpRequestBodyWriter.write(data)
        httpRequestBodyWriter.close()
        val input = BufferedInputStream(con.inputStream)
        val br = BufferedReader(InputStreamReader(input))
        val sb = StringBuffer()
        var inputLine = ""
        for (inputLine in br.readLine()) {
            sb.append(inputLine)
        }
        return HttpResponse(sb.toString(), con)
    }
}

class HttpResponse {
    private var data :String
    private var con  :HttpURLConnection;

    constructor(data: String,con :HttpURLConnection){
        this.data = data
        this.con = con

    }

    fun GetData():String{
        return this.data
    }

    fun GetConnection():HttpURLConnection{
        return  this.con
    }

}
