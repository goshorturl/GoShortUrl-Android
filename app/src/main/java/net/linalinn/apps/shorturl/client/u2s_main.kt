package net.linalinn.apps.shorturl.client

import android.os.Bundle
import android.util.Log
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import android.view.MenuItem
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.navigation.NavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.view.Menu
import android.view.View
import net.linalinn.apps.shorturl.client.services.ServiceListFragment
import net.linalinn.apps.shorturl.client.shorturl.Shorturls
import net.linalinn.apps.shorturl.client.R
import android.content.Context
import android.content.Intent


class u2s_main : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, View.OnLayoutChangeListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_u2s_main)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val sharedPref = applicationContext.getSharedPreferences("net.linalinn.apps.shorturl.client.sharedpreferences", Context.MODE_PRIVATE)
        val init = sharedPref.getBoolean("init", false)
        if(init == false ){
            val intent = Intent(this, Init::class.java)
            this.startActivity(intent)
        }

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        val shorturlsFragment = Shorturls()
        fragmentTransaction.replace(R.id.framentContainer,shorturlsFragment)
        fragmentTransaction.commit();

    }

    override fun onLayoutChange(v: View?, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int, oldTop: Int, oldRight: Int, oldBottom: Int) {
        Log.d("Main","onLayoutChange")
        v?.rootView?.let { Snackbar.make(it,"LOL LOL",Snackbar.LENGTH_LONG).show() }
        if (v?.id == R.id.framentContainer){
            Snackbar.make(v.rootView,"Lol",Snackbar.LENGTH_LONG).show()
            Snackbar.make(v,"LOL LOL",Snackbar.LENGTH_LONG).show()
        }
    }
    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.u2s_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.menu_services -> {
                val serviceListFragment = ServiceListFragment()
                fragmentTransaction.replace(R.id.framentContainer,serviceListFragment)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()

            }
            R.id.menu_shorturls -> {
                val shorturlsFragment = Shorturls()
                fragmentTransaction.replace(R.id.framentContainer,shorturlsFragment)
                fragmentTransaction.addToBackStack(null)
                fragmentTransaction.commit()

            }/*
            R.id.nav_slideshow -> {


            }
            R.id.nav_tools -> {


            }
            R.id.nav_share -> {


            }
            R.id.nav_send -> {

            }*/
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }
}
