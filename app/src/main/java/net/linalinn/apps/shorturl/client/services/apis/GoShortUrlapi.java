package net.linalinn.apps.shorturl.client.services.apis;

import android.util.Log;

import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;

/**
 * Created by linalinn on 14.07.19.
 */

public class GoShortUrlapi implements Api {

    public String shortUrl(String server, String ApiKey, String URL) throws Exception{
        HttpResponse response = SimpleHttp.doPost(new URL(server + "/api/go/v1/add"),"url="+URL+"&key="+ApiKey);
        if (response.GetConnection().getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST){
            throw new Exception("malformed URL");
        }
        return response.GetData();
    }

    public String shortUrl(String server, String ApiKey, String URL, String Coustom) throws Exception{
        HttpResponse response = SimpleHttp.doPost(new URL(server + "/api/go/v1/add"),"url="+URL+"&shorturl="+Coustom+"&key="+ApiKey);
        if (response.GetConnection().getResponseCode() == HttpURLConnection.HTTP_BAD_REQUEST){
            throw new Exception("malformed URL");
        }
        return response.GetData();

    }

    public Boolean isUsed(String server, String ApiKey, String Coustom) throws Exception {
        HttpResponse response = SimpleHttp.doPost(new URL(server + "/api/go/v1/lookup"),"shorturl="+Coustom+"&key="+ApiKey);
        Log.d("GoShortUrlapi",server + "/api/go/v1/lookup" + response.GetConnection().getResponseCode());
        int responseCode = response.GetConnection().getResponseCode();
        if(responseCode == 200 ){
            return true;
        } else if (responseCode == 404){
            return false;
        } else if (responseCode == HttpURLConnection.HTTP_UNAUTHORIZED){
            throw new Exception("Invalid ApiKey");
        } else if (responseCode == HttpURLConnection.HTTP_INTERNAL_ERROR){
            throw new Exception("Internal Server Error");
        } else {
            throw new Exception("Unknown Error");
        }
    }

    public String list(String server, String ApiKey) throws Exception {
        HttpResponse response = SimpleHttp.doPost(new URL(server + "/api/go/v1/list"),"key="+ApiKey);
        int responseCode = response.GetConnection().getResponseCode();
        if(responseCode == 200 ){
            return response.GetData();
        } else if (responseCode == HttpURLConnection.HTTP_UNAUTHORIZED){
            throw new Exception("Invalid ApiKey");
        } else if (responseCode == HttpURLConnection.HTTP_INTERNAL_ERROR){
            throw new Exception("Internal Server Error");
        } else {
            throw new Exception("Unknown Error");
        }
    }

    @Override
    public void update(String server, String ApiKey) throws Exception {

    }

    public String GetApiKey(String server,String Username,String Password) throws Exception{
        HttpResponse response = SimpleHttp.doPost(new URL(server+"/api/go/v1/get_new_api_key"),"username="+Username+"&passwd="+Password);
        int responseCode = response.GetConnection().getResponseCode();
        if(responseCode == 200 ){
            return response.GetData();
        } else if (responseCode == HttpURLConnection.HTTP_UNAUTHORIZED){
            throw new Exception("Invalid ApiKey");
        } else if (responseCode == HttpURLConnection.HTTP_INTERNAL_ERROR){
            throw new Exception("Internal Server Error");
        } else {
            throw new Exception("Unknown Error");
        }

    }

    public String lookup(String server, String ApiKey,String ShortUrl) throws Exception {
        HttpResponse response = SimpleHttp.doPost(new URL(server + "/api/go/v1/info"),"key="+ApiKey + "&shorturl="+ShortUrl);
        int responseCode = response.GetConnection().getResponseCode();
        if(responseCode == 200 ){
            return response.GetData();
        } else if (responseCode == HttpURLConnection.HTTP_UNAUTHORIZED){
            throw new Exception("Invalid ApiKey");
        } else if (responseCode == HttpURLConnection.HTTP_INTERNAL_ERROR){
            throw new Exception("Internal Server Error");
        } else {
            throw new Exception("Unknown Error");
        }
    }

    public String info(String server, String ApiKey, String ShortUrl) throws Exception {
        HttpResponse response = SimpleHttp.doPost(new URL(server + "/api/go/v1/info"),"key="+ApiKey + "&shorturl="+ShortUrl);
        int responseCode = response.GetConnection().getResponseCode();
        if(responseCode == 200 ){
            return response.GetData();
        } else if (responseCode == HttpURLConnection.HTTP_UNAUTHORIZED){
            throw new Exception("Invalid ApiKey");
        } else if (responseCode == HttpURLConnection.HTTP_INTERNAL_ERROR){
            throw new Exception("Internal Server Error");
        } else {
            throw new Exception("Unknown Error");
        }
    }
}
