package net.linalinn.apps.shorturl.client.shorturl;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import net.linalinn.apps.shorturl.client.R;
import net.linalinn.apps.shorturl.client.services.apis.Api;
import net.linalinn.apps.shorturl.client.services.apis.GoShortUrlapi;
import net.linalinn.apps.shorturl.client.services.apis.Polrapi;
import net.linalinn.apps.shorturl.client.services.database.Service;
import net.linalinn.apps.shorturl.client.services.database.ServiceDatabase;
import net.linalinn.apps.shorturl.client.shorturl.database.Shorturl;
import net.linalinn.apps.shorturl.client.shorturl.database.ShorturlDatabase;

import java.util.ArrayList;
import java.util.HashMap;


public class Short extends Fragment implements View.OnClickListener {
    private HashMap<String,Service> serviceHashMap;
    private ArrayList<CharSequence> arrayList;
    private EditText URL_EditText;
    private EditText RURL_EditText;
    private TextView shorturl_TextView;
    private Spinner spinner;



    public Short() {
        // Required empty public constructor
        serviceHashMap = new HashMap<>();
        arrayList = new ArrayList<>();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_short, container, false);
        spinner  = (Spinner) rootView.findViewById(R.id.spinner);
        URL_EditText        = (EditText)    rootView.findViewById(R.id.URL);
        RURL_EditText       = (EditText)    rootView.findViewById(R.id.RURL);
        shorturl_TextView   = (TextView)    rootView.findViewById(R.id.shorturl);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);




        ServiceDatabase db = Room.databaseBuilder(getContext(),ServiceDatabase.class,"services.db").allowMainThreadQueries().build();
        for (Service service : db.servicelDao().loadAllServices()) {
            String text = service.getUsername()+ " @ " +service.getServerUrl().replace("http://","").replace("https://","");
            arrayList.add(text);
            serviceHashMap.put(text,service);
        }
        ArrayAdapter<CharSequence> arrayAdapter = new ArrayAdapter<>(getContext(),android.R.layout.simple_spinner_item,arrayList);//ArrayAdapter.createFromResource(getContext(), R.array.no ,android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        ((Button) rootView.findViewById(R.id.button_short)).setOnClickListener(this);
        return  rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

    }

    @Override
    public void onClick(View v) {

        if (R.id.button_sahre == v.getId() && (shorturl_TextView.getText().toString() != "Short URL")){
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, shorturl_TextView.getText().toString());
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        }


        if(serviceHashMap.keySet().contains(spinner.getSelectedItem().toString())){
            Service s = serviceHashMap.get(spinner.getSelectedItem().toString());
            Api api;
            ShorturlDatabase db  = Room.databaseBuilder(getContext().getApplicationContext(), ShorturlDatabase.class,"Shorturl.db").allowMainThreadQueries().build();
            Log.d("Short",s.getServerUrl());
            Log.d("Short Rurl",RURL_EditText.getText().toString());
            Log.d("Short url",URL_EditText.getText().toString());

            if( s.getApiType() == 2){
                api = new Polrapi();
            } else {
                api = new GoShortUrlapi();
            }
            if (RURL_EditText.getText().toString().length() != 0){
                try {
                    Log.d("Short",Boolean.toString(api.isUsed(s.getServerUrl(),s.getAccessToken(),RURL_EditText.getText().toString())));
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("NETWORKMain",e.toString());
                }

                try {

                    if (!api.isUsed(s.getServerUrl(),s.getAccessToken(),RURL_EditText.getText().toString())) {
                        String url = api.shortUrl(s.getServerUrl(), s.getAccessToken(), URL_EditText.getText().toString(), RURL_EditText.getText().toString());
                        db.shorturlDao().insertShorturls(new Shorturl(url,URL_EditText.getText().toString(),"Polr",0));
                        shorturl_TextView.setText(url);
                        Log.d("Short result",url);
                    } else {
                        shorturl_TextView.setText(R.string.url_in_use);

                    }

                } catch (Exception e) {

                    e.printStackTrace();
                    Log.e("NETWORKMain",e.toString());
                    if(e.getMessage() == "malformed URL"){
                        shorturl_TextView.setText(R.string.malformed_url);
                    }

                }

            } else {

                try {
                    String url = api.shortUrl(s.getServerUrl(),s.getAccessToken(),URL_EditText.getText().toString());
                    Log.d("Short result random",url);
                    db.shorturlDao().insertShorturls(new Shorturl(url,URL_EditText.getText().toString(),"Polr",0));
                    shorturl_TextView.setText(url);
                } catch (Exception e) {
                    if(e.getMessage() == "malformed URL"){
                        shorturl_TextView.setText(R.string.malformed_url);
                    }
                    e.printStackTrace();
                }

            }
            shorturl_TextView.setVisibility(View.VISIBLE);


        }

    }
}
