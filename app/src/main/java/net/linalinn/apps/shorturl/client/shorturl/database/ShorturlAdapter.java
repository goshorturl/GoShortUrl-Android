package net.linalinn.apps.shorturl.client.shorturl.database;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import net.linalinn.apps.shorturl.client.R;

import java.util.List;

public class ShorturlAdapter extends RecyclerView.Adapter<ShorturlAdapter.ViewHolder> {
    private List<Shorturl> Shorturls;
    private  FragmentManager fragmentManager;

    @NonNull
    @Override
    public ShorturlAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View shorturlView = inflater.inflate(R.layout.item_shorturl, parent, false);
        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(shorturlView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ShorturlAdapter.ViewHolder holder, int position) {
        Shorturl shorturl = Shorturls.get(position);
        holder.Shorturl.setText(shorturl.getShortUrl());
        holder.Clicks.setText(Integer.toString(shorturl.getClicks()));
        holder.Url.setText(shorturl.getUrl());
        holder.Service.setText(shorturl.getService());


    }


    public ShorturlAdapter(List<Shorturl> shorturls, FragmentManager supportFragmentManager) {
        Shorturls = shorturls;
        fragmentManager = supportFragmentManager;
    }

    @Override
    public int getItemCount() {
        return Shorturls.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView Shorturl;
        public TextView Clicks;
        public TextView Url;
        public TextView Service;


        public ViewHolder(@NonNull final View itemView) {
            super(itemView);
            Shorturl = (TextView) itemView.findViewById(R.id.shorturl);
            Clicks = (TextView) itemView.findViewById(R.id.item_clicks);
            Url = (TextView) itemView.findViewById(R.id.item_url);
            Service = (TextView) itemView.findViewById(R.id.item_service);
        }
    }
}
