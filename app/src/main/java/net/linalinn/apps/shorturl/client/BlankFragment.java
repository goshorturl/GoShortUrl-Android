package net.linalinn.apps.shorturl.client;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.material.snackbar.Snackbar;

import net.linalinn.apps.shorturl.client.services.apis.GoShortUrlapi;
import net.linalinn.apps.shorturl.client.services.database.Service;
import net.linalinn.apps.shorturl.client.services.database.ServiceDatabase;

import java.net.MalformedURLException;
import java.net.URL;


public class BlankFragment extends Fragment {
    private EditText url;
    private EditText name;
    private EditText pass;
    private Button save;
    private Spinner services;


    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_blank, container, false);
    }
    private boolean checkFields(){
        Boolean result = true;
        if (name.getText().toString().length() == 0){
            name.setError("required");
            result = false;
        }
        if (url.getText().toString().length() == 0){
            url.setError("required");
            result = false;
        } else {
            try {
                new URL(url.getText().toString());
            } catch (MalformedURLException e) {
                url.setError(getString(R.string.malformed_url));
            }
        }
        if (pass.getText().toString().length() == 0){
            pass.setError("required");
            result = false;
        }

        return result;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        url = view.findViewById(R.id.lol1);
        name = view.findViewById(R.id.lol2);
        pass = view.findViewById(R.id.lol3);
        save = view.findViewById(R.id.lol4);
        services = view.findViewById(R.id.spinner5);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServiceDatabase db = Room.databaseBuilder(getContext(),ServiceDatabase.class,"services.db").allowMainThreadQueries().build();
                db.servicelDao().insertServiceDao(new Service(name.getText().toString(),url.getText().toString(),pass.getText().toString(), 2));
                Log.d("ServiceAddFragment","spinner " + services.getSelectedItem().toString());
                String Username = name.getText().toString();
                String server = url.getText().toString();
                String pw = pass.getText().toString();


                if ("choose service".contains(services.getSelectedItem().toString())){
                    Snackbar.make(v,"Selcet a service",Snackbar.LENGTH_LONG);

                }
                if ("Url2Shorturl.cloud".contains(services.getSelectedItem().toString())){
                    GoShortUrlapi api = new GoShortUrlapi();
                    Log.d("ServiceAddFragment","Url2ShortUrl.cloud");
                    //db = Room.databaseBuilder(getContext(),ServiceDatabase.class,"services.db").allowMainThreadQueries().build();
                    String token;
                    try {
                        token = api.GetApiKey(server,server,pw);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                    if(checkFields()){
                        db.servicelDao().insertServiceDao(new Service(Username,server,token, 0));

                    } else {
                        return;
                    }


                }
                if ("GoShortUrl".contains(services.getSelectedItem().toString())){
                    GoShortUrlapi api = new GoShortUrlapi();
                    Log.d("ServiceAddFragment","GoShortUrl");
                    //db = Room.databaseBuilder(getContext(),ServiceDatabase.class,"services.db").allowMainThreadQueries().build();
                    String token;
                    try {
                        token = api.GetApiKey(server,server,pw);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return;
                    }
                    if(checkFields()){
                        db.servicelDao().insertServiceDao(new Service(Username,server,token, 1));
                    } else {
                        return;
                    }


                }
                if ("Polr".contains(services.getSelectedItem().toString())){
                    Log.d("ServiceAddFragment","Polr");
                    //db = Room.databaseBuilder(getContext(),ServiceDatabase.class,"services.db").allowMainThreadQueries().build();
                    if(checkFields()){
                        db.servicelDao().insertServiceDao(new Service(Username,server,pw, 2));
                    } else {
                        return;
                    }

                }

                getFragmentManager().popBackStackImmediate();
            }
        });
    }
}
