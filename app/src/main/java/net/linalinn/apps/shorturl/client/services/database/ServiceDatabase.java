package net.linalinn.apps.shorturl.client.services.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;


@Database(entities = {Service.class}, version = 2,exportSchema = false )
public abstract class ServiceDatabase extends RoomDatabase {

    private static ServiceDatabase INSTANCE;
    public abstract ServiceDao servicelDao();
}
