package net.linalinn.apps.shorturl.client;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import net.linalinn.apps.shorturl.client.services.apis.Api;
import net.linalinn.apps.shorturl.client.services.apis.GoShortUrlapi;
import net.linalinn.apps.shorturl.client.services.apis.Polrapi;
import net.linalinn.apps.shorturl.client.services.database.Service;
import net.linalinn.apps.shorturl.client.services.database.ServiceDatabase;
import net.linalinn.apps.shorturl.client.shorturl.database.Shorturl;
import net.linalinn.apps.shorturl.client.shorturl.database.ShorturlDatabase;

import java.util.ArrayList;
import java.util.HashMap;

public class Share_short extends AppCompatActivity implements View.OnClickListener {

    private HashMap<String, Service> serviceHashMap;
    private ArrayList<CharSequence> arrayList;
    private EditText URL_EditText;
    private EditText RURL_EditText;
    private TextView shorturl_TextView;
    private Spinner spinner;

    @Override
    protected void onResume() {
        super.onResume();


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        ServiceDatabase db = Room.databaseBuilder(this,ServiceDatabase.class,"services.db").allowMainThreadQueries().build();
        for (Service service : db.servicelDao().loadAllServices()) {
            String text = service.getUsername()+ " @ " +service.getServerUrl().replace("http://","").replace("https://","");
            arrayList.add(text);
            serviceHashMap.put(text,service);
        }
        ArrayAdapter<CharSequence> arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,arrayList);//ArrayAdapter.createFromResource(getContext(), R.array.no ,android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_short);
        spinner  = (Spinner) findViewById(R.id.spinner);
        URL_EditText        = (EditText)    findViewById(R.id.URL);
        RURL_EditText       = (EditText)    findViewById(R.id.RURL);
        shorturl_TextView   = (TextView)    findViewById(R.id.shorturl);
        serviceHashMap = new HashMap<>();
        arrayList = new ArrayList<>();


        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        ServiceDatabase db = Room.databaseBuilder(this,ServiceDatabase.class,"services.db").allowMainThreadQueries().build();
        for (Service service : db.servicelDao().loadAllServices()) {
            String text = service.getUsername()+ " @ " +service.getServerUrl().replace("http://","").replace("https://","");
            arrayList.add(text);
            serviceHashMap.put(text,service);
        }
        ArrayAdapter<CharSequence> arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_spinner_item,arrayList);//ArrayAdapter.createFromResource(getContext(), R.array.no ,android.R.layout.simple_spinner_item);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        ((Button) findViewById(R.id.button_short)).setOnClickListener(this);

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
            SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(getString(R.string.sharedpreferences), Context.MODE_PRIVATE);
            if(!sharedPref.getBoolean("init",false)){
                Intent intent1 = new Intent(this,Init.class);
                startActivity(intent1);
            }

            URL_EditText.setText(sharedText);


        }

    }

    @Override
    public void onClick(View v) {

        if (R.id.button_sahre == v.getId() && (shorturl_TextView.getText().toString() != "Short URL")){
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, shorturl_TextView.getText().toString());
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        }


        if(serviceHashMap.keySet().contains(spinner.getSelectedItem().toString())) {
            Service s = serviceHashMap.get(spinner.getSelectedItem().toString());
            Api api;
            ShorturlDatabase db = Room.databaseBuilder(v.getContext().getApplicationContext(), ShorturlDatabase.class, "Shorturl.db").allowMainThreadQueries().build();

            if (s.getApiType() == 2) {
                api = new Polrapi();
            } else {
                api = new GoShortUrlapi();
            }
            if (RURL_EditText.getText().toString().length() != 0) {
                try {
                    Log.d("Short", Boolean.toString(api.isUsed(s.getServerUrl(), s.getAccessToken(), RURL_EditText.getText().toString())));
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("NETWORKMain", e.toString());
                }

                try {

                    if (!api.isUsed(s.getServerUrl(), s.getAccessToken(), RURL_EditText.getText().toString())) {
                        String url = api.shortUrl(s.getServerUrl(), s.getAccessToken(), URL_EditText.getText().toString(), RURL_EditText.getText().toString());
                        db.shorturlDao().insertShorturls(new Shorturl(url, URL_EditText.getText().toString(), "Polr", 0));
                        shorturl_TextView.setText(url);
                        Log.d("Short result", url);
                    } else {
                        shorturl_TextView.setText(R.string.url_in_use);
                        shorturl_TextView.setVisibility(View.VISIBLE);

                    }

                } catch (Exception e) {

                    e.printStackTrace();
                    Log.e("NETWORKMain", e.toString());
                    if (e.getMessage() == "malformed URL") {
                        shorturl_TextView.setText(R.string.malformed_url);
                        shorturl_TextView.setVisibility(View.VISIBLE);
                    }

                }

            } else {

                try {
                    String url = api.shortUrl(s.getServerUrl(), s.getAccessToken(), URL_EditText.getText().toString());
                    Log.d("Short result random", url);
                    db.shorturlDao().insertShorturls(new Shorturl(url, URL_EditText.getText().toString(), "Polr", 0));
                    shorturl_TextView.setText(url);
                } catch (Exception e) {
                    if (e.getMessage() == "malformed URL") {
                        shorturl_TextView.setText(R.string.malformed_url);
                        shorturl_TextView.setVisibility(View.VISIBLE);
                    }
                    e.printStackTrace();
                }

            }

        }

    }
}

