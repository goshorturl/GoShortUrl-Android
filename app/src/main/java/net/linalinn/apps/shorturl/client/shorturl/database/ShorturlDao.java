package net.linalinn.apps.shorturl.client.shorturl.database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

@Dao
public interface ShorturlDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertShorturls(Shorturl... shorturls);
    @Update
    public void updateShorturls(Shorturl... shorturls);
    @Delete
    public void deleteShorturls(Shorturl... shorturls);
    @Query("SELECT * FROM shorturl")
    public Shorturl[] loadAllShorturls();

}
