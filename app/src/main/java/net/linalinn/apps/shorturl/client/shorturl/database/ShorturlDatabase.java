package net.linalinn.apps.shorturl.client.shorturl.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
@Database(entities = {Shorturl.class}, version = 2,exportSchema = false )
public abstract class ShorturlDatabase extends RoomDatabase {

    private static ShorturlDatabase INSTANCE;
    public abstract  ShorturlDao shorturlDao();
}
