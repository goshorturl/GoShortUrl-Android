package net.linalinn.apps.shorturl.client.services.apis;

public class SupportedApis {
    public static int u2s = 0;
    public static int GoShortUrl = 1;
    public static int Polr = 2;
    private static String _0 = "u2s";
    private static String _1 = "GoShortUrl";
    private static String _2 = "Polr";

    public static String getName(int id){
        if (id == 0){
            return _0;
        }
        if (id == 1){
            return _1;
        }
        if (id == 2){
            return _2;
        }
        return "";
    }
    public static int getId(String Name){
        if (Name == "u2s"){
            return 0;
        }
        if (Name == "GoShortUrl"){
            return 1;
        }
        if (Name == "Polr"){
            return 2;
        }
        return -1;
    }

}
