package net.linalinn.apps.shorturl.client.services.apis;

public interface Api {
    String shortUrl (String server, String ApiKey, String URL) throws Exception;
    String shortUrl(String server, String ApiKey, String URL, String Coustom) throws Exception;
    Boolean isUsed(String server, String ApiKey, String Coustom) throws Exception;
    String list(String server, String ApiKey) throws Exception;
    void update(String server, String ApiKey) throws Exception;

}
