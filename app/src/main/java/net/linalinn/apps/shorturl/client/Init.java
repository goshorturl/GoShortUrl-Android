package net.linalinn.apps.shorturl.client;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import net.linalinn.apps.shorturl.client.services.apis.SupportedApis;
import net.linalinn.apps.shorturl.client.services.database.Service;
import net.linalinn.apps.shorturl.client.services.database.ServiceDatabase;

import java.net.MalformedURLException;
import java.net.URL;

public class Init extends AppCompatActivity implements View.OnClickListener {
    private Button Save;
    private RadioButton GoShortUrl;
    private RadioButton Polr;
    private EditText Url;
    private EditText Name;
    private EditText Password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);
        Intent intent = getIntent();
        Log.d("Init","Start Init Activity");
        Save = (Button) findViewById(R.id.save);
        GoShortUrl = (RadioButton) findViewById(R.id.radioGoShortUrl);
        Polr = (RadioButton) findViewById(R.id.radioPlor);
        Url = (EditText) findViewById(R.id.url);
        Name = (EditText) findViewById(R.id.name);
        Password = (EditText) findViewById(R.id.password);
        Save.setOnClickListener(this);
        GoShortUrl.setOnClickListener(this);
        Polr.setOnClickListener(this);

    }

    private boolean checkFields(){
        Boolean result = true;
        if (Name.getText().toString().length() == 0){
            Name.setError("required");
            result = false;
        }
        if (Url.getText().toString().length() == 0){
            Url.setError("required");
            result = false;
        } else {
            try {
                URL url = new URL(Url.getText().toString());
            } catch (MalformedURLException e) {
                Url.setError(getString(R.string.malformed_url));
            }
        }
        if (Password.getText().toString().length() == 0){
            Password.setError("required");
            result = false;
        }

        return result;
    }

    @Override
    public void onClick(View v) {
        Log.d("Init","onClick");
        Log.d("Init","onClick ID" + v.getId());
        if(v.getId() == R.id.radioPlor){
            Log.d("Init","onClick:radioPlor");
            Password.setHint("Api Key");

        } else if(v.getId() == R.id.radioGoShortUrl){
            Log.d("Init","onClick:radioGoShortUrl");
            Password.setHint(R.string.password);


        } else if(v.getId() == R.id.save){
            Log.d("Init","onClick:save");
            ServiceDatabase db = Room.databaseBuilder(v.getContext(),ServiceDatabase.class,"services.db").allowMainThreadQueries().build();
            Log.d("Init","GoShortUrl.isChecked() " + Boolean.toString(GoShortUrl.isChecked()));
            Log.d("Init","Polr.isChecked() " + Boolean.toString(Polr.isChecked()));
            if (GoShortUrl.isChecked()){
                Log.d("Init","GoShortUrl.isSelected()");
                db.servicelDao().insertServiceDao(new Service(Name.getText().toString(),Url.getText().toString(),Password.getText().toString(), SupportedApis.GoShortUrl));
                Log.d("Init","finish");
                if(checkFields()){
                     v.getContext().getApplicationContext().getSharedPreferences("net.linalinn.apps.shorturl.client.sharedpreferences", Context.MODE_PRIVATE).edit().putBoolean("init", true).commit();
                    super.finish();
                }

            } else  if (Polr.isChecked()){
                Log.d("Init","Polr.isSelected()");
                db.servicelDao().insertServiceDao(new Service(Name.getText().toString(),Url.getText().toString(),Password.getText().toString(), SupportedApis.Polr));
                Log.d("Init","finish");
                if(checkFields()){
                    v.getContext().getApplicationContext().getSharedPreferences("net.linalinn.apps.shorturl.client.sharedpreferences", Context.MODE_PRIVATE).edit().putBoolean("init", true).commit();
                    super.finish();
                }
            }




        }
    }
}
